console.log(
[1, 2, 3].flatMap((x) => [
  String.fromCharCode(64 + x),
  String.fromCharCode(96 + x)
])
);

['A', 'a', 'B', 'b', 'C', 'c']