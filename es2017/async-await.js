async function foo() {
  return new Promise((resolve) =>
    setTimeout(() => resolve('done'), 1000)
  );
}

async function bar() {
  const result = await foo();

  console.log(result.toUpperCase());
}

bar();