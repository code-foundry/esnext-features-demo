Object.getOwnPropertyDescriptors({ a: 1, b: true, c: 'hi' })

const x = {
  a: { value: 1, writable: true, enumerable: true, configurable: true },
  b: { value: true, writable: true, enumerable: true, configurable: true },
  c: { value: 'hi', writable: true, enumerable: true, configurable: true }
}