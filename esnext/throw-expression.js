[1, 2, 3, 4, 5].map(
  (x) => x < 5 ? x + 1 : throw new Error('too large!')
);