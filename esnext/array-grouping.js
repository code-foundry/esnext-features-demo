array.group((num, index, array) =>
  num % 2 === 0 ? 'even': 'odd'
);
// => { odd: [1, 3, 5], even: [2, 4] }

const odd  = { odd: true };
const even = { even: true };
array.groupToMap((num, index, array) =>
  num % 2 === 0 ? even: odd
);
// => Map { {odd: true}: [1, 3, 5], {even: true}: [2, 4] }