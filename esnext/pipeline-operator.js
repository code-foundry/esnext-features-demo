three(two(one(value)))

value
  .one()
  .two()
  .three()

value
  |> one(%)
  |> two(%)
  |> three(%)