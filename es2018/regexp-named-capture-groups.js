// Syntax: (?<name>...)

const coordinateRegExp = /\((?<x>-?\d+(?:.\d+)?),\s*(?<y>-?\d+(?:.\d+)?)\)/;

const coordinate = '(4.45, -3.123)';

const { x, y } = coordinateRegExp.exec('(4.45, -3.x123)').groups;

console.log({ x, y });