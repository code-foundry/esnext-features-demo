new Promise(
  (resolve, reject) => Math.random() < 0.5 ? resolve() : reject()
).then(
  () => console.log('Ok')
).catch(
  () => console.log('Error')
).finally(
  () => console.log('Done')
);