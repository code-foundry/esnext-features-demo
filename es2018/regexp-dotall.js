const message = 'Hello\nWorld';

/Hello.+/.test(message);  // false
/Hello.+/s.test(message); // true