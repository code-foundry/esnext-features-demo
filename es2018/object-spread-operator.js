const foo = {
  a: '1',
  b: 2
};

const bar = {
  ...foo,
  c: true
}