class Message {
  #message = 'T0p 53cr3t!'

  revealMessage() { console.log(this.#message); }
}

const message = new Message();
message.revealMessage();
console.log(message['#message']); // undefined