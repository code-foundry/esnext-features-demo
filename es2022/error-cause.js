const error = new Error(
  'Overflow',
  { cause: new Error('Ate too much') },
);

console.log(error.cause);