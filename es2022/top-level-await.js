async function getData() {
  return new Promise(
    (resolve) => setTimeout(resolve, 1000, 'Howdy!')
  );
}

const message = await getData();

console.log(message);
