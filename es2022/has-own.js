const myObj = { prop: 'exists' };
  
console.log(Object.hasOwn(myObj, 'prop'));
// output: true

console.log(Object.hasOwn(myObj, 'toString'));
// output: false

console.log(Object.hasOwn(myObj, 'undeclaredPropertyValue'));
// output: false