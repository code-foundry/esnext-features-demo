let x = null;

x ??= false;
x ||= Math.random() < 0.5;
x &&= Math.random() < 0.5;