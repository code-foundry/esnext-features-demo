const oopsie = new AggregateError([
  new Error('Gas tank leak'),
  new Error('Stepped on a lego'),
  new Error('Bought Bitcoin'),
], 'Several things went wrong');

console.log(oopsie.message, oopsie.errors);