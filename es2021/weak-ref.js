// Run with the `--expose_gc` flag: `node --expose_gc es2021/weak-ref.js`

function foo() {
  return new WeakRef({});
}
  
const ref = foo();

setTimeout(() => console.log(ref.deref()), 5000);

// Output: {} or undefined

setTimeout(() => global.gc(), 1000);
  