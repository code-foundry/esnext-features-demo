const finalizationRegistry = new FinalizationRegistry((value) => console.log('cleaning up:', value));

const referencedValue = {};

finalizationRegistry.register({}, 'unreferenced value');

(function() {
    const delayedUnreferencedValue = {};
    finalizationRegistry.register(delayedUnreferencedValue, 'delayed unreferenced value');
    setTimeout(() => delayedUnreferencedValue.toString(), 2000);
})();

finalizationRegistry.register(referencedValue, 'referenced value');

setTimeout(() => referencedValue.toString(), 10000);
