const promise1 = Promise.resolve('ok');
const promise2 = Promise.reject('oops');
const promise3 = new Promise((resolve, reject) => setTimeout(reject, 1000, 'delayed oopsie'));

Promise.allSettled([promise1, promise2, promise3]).then(
  (results) => results.forEach((result) => console.log(result.status))
);

// Output:
// fulfilled
// rejected
// rejected